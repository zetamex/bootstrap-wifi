How To Install
=====

1. Download the latest version of Diva Distro or Diva Wifi for Robust.
2. Delete the WifiPages folder
3. Place this WifiPages folder where the old one was
4. Start the standalone or robust instance