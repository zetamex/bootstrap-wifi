Read Me
========
This is the first beta release of Bootstrap Wifi, released under the Creative Commons License.

Change Log
========
v 0.1
-----
**New Features**  
* Latest version of bootstrap  
* Uses Models for login  
**Known Issues**  
* Inventory Doesn't look correctly but works  
* Web console appears broken currently  